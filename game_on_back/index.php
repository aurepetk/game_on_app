<?php 

include_once 'Router.php';
include_once 'Request.php';

set_error_handler(
    function ($severity, $message, $file, $line) {
        throw new ErrorException($message, $severity, $severity, $file, $line);
    }
);

$router = new Router(new Request);

$router->get('/', function() {
    header("Access-Control-Allow-Origin: *");
    $mainData = getMainData();
    return json_encode($mainData);
});

$router->get('/teams', function($request) {
    header("Access-Control-Allow-Origin: *");
    $mainData = getMainData();
    
    if (is_array($mainData) && !empty($mainData)) {
        $teamsIds = getTeamsIds($mainData, 'teamAId', 'teamBId');
        $teams = getTeamsNames($teamsIds);
    } else {
        $teams = [];
    }

    return json_encode($teams);
});

function getMainData() {
    try {
        $json = file_get_contents('http://82.135.146.98/test-services/scheduleAndResults.php');
        return json_decode($json); 
    }
    catch (Exception $e) {
        logErrors($e);
        return array();
    }
}

function getTeamsIds($array, $key1, $key2) { 
    $uniqueIds = array(); 
    
    foreach($array as $row) {
        if (!in_array($row->$key1, $uniqueIds)) { 
            array_push($uniqueIds, $row->$key1); 
        }
        if (!in_array($row->$key2, $uniqueIds)) {
            array_push($uniqueIds, $row->$key2);
        }         
    } 

    return $uniqueIds; 
} 

function getTeamsNames($array) {
    $teams = array();

    foreach($array as $id) {
        try {
            $json = file_get_contents('http://82.135.146.98/test-services/team.php?id='.$id);
            $team = json_decode($json);
            array_push($teams, $team);
        }
        catch (Exception $e) {
            logErrors($e);
            $teams = [];
            break;
        }
    }

    return $teams;
}

function logErrors($e) {
    $error = $e->getMessage();
    error_log(date("Y-m-d H:i:s")." ".$error, 3, '/home/vagrant/gameOn/error.log');
}

?>