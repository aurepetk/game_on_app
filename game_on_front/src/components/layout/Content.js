import React from 'react';
import axios from 'axios';
import './Content.css';
import Table from '../tables/Table';

class Content extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            data: [],
            matches: [],
            teams: [],
            sortBy: 'desc'
        };
    };
    
    sortBy = (key, table) => {
        let arrayCopy = [];
        if (table === 'Matches') {
            arrayCopy = [...this.state.matches];
        }else {
            arrayCopy = [...this.state.teams];
        };

        if (this.state.sortBy === 'asc') {
            this.setState({sortBy: 'desc'});
            arrayCopy.sort(this.compareByAsc(key));
        } else {
            this.setState({sortBy: 'asc'});
            arrayCopy.sort(this.compareByDesc(key));
        };

        if (table === 'Matches') {
            this.setState({matches: arrayCopy});
        }else {
            this.setState({teams: arrayCopy});
        };        
    };

    compareByAsc(key) {
        return function (a, b) {
            if (a[key] < b[key]) return -1;
            if (a[key] > b[key]) return 1;
            return 0;
        };
    };

    compareByDesc(key) {
        return function (a, b) {
            if (a[key] > b[key]) return -1;
            if (a[key] < b[key]) return 1;
            return 0;
        };
    };

    handleHTTPRequestErrors(error) {
        if (error.response) {
            console.log('Error response data: ', error.response.data);
            console.log('Error response status: ', error.response.status);
            console.log('Error response headers: ', error.response.headers);
        } else if (error.request) {
            console.log('Error: no response from the server: ', error.request);
        } else {
            console.log('Error:', error.message);
        }
        console.log('Error config: ', error.config);
    }

    componentWillMount() {
        this.getMainData();
    };

    getMainData = () => {
        axios.get(this.props.matchesURL, { crossDomain: true })
            .then(response => {
                if (Array.isArray(response.data) && response.data.length !== 0) {
                    this.setState( { data: response.data });
                    this.formTablesData();   
                } else {
                    this.setState({ matches: [], teams: []});
                }     
            })
            .catch(error => {
                this.handleHTTPRequestErrors(error);
            });
    };

    async formTablesData() {
        let matches = [];
        let victories = [];
        let teams = await this.getTeamsNames();
        if (Array.isArray(teams) && teams.length !== 0) {
            this.state.data.forEach(rowData =>  {
                let teamAName = this.findTeamName(rowData.teamAId, teams);
                let teamBName = this.findTeamName(rowData.teamBId, teams);
                let score;
                if (rowData.scoreA === undefined || rowData.scoreB === undefined) {
                    score =  'No final score yet';
                } else {
                    score = rowData.scoreA+':'+rowData.scoreB;
                    if (rowData.scoreA > rowData.scoreB) {
                        victories.push(teamAName);
                    } else {
                        victories.push(teamBName);
                    };
                };
                matches.push({id: rowData.id, date: rowData.date, teamA: teamAName, teamB: teamBName, score });
            });
            let teamsWins = this.countVictories(victories);
            let teamsData = this.addTeamsWithoutWins(teamsWins, teams);
            this.setState({ matches: matches, teams: teamsData});
        };
    };

    async getTeamsNames() {
        try {
            const response = await axios.get(this.props.teamsURL, { crossDomain: true });
            return response.data;
        } catch(error) {
            this.handleHTTPRequestErrors(error);
            return [];
        }
    };

    findTeamName(teamId, teams) {
        for(let i = 0; i < teams.length; i++) {
            if (teams[i].id === teamId) {
                return teams[i].name;
            }
        };
    };

    countVictories(victories) {    
        victories.sort();
        let teamsData = [];
        let team = null;
        let wins = 0;
        for (var i = 0; i <= victories.length; i++) {
            if (victories[i] !== team) {
                if (wins > 0) {
                    teamsData.push({ name: team, wins: wins});
                };
                team = victories[i];
                wins = 1;
            } else {
                wins++;
            };
        };  
        return teamsData; 
    };

    addTeamsWithoutWins(teamsWins, allTeams) {
        for(let i = 0; i < allTeams.length; i++) {
            var found = false;
            for(let j = 0; j < teamsWins.length; j++) {
                if (teamsWins[j].name === allTeams[i].name){
                    found = true;
                    break;
                };
            };
            if (found === false) {
                teamsWins.push({ name: allTeams[i].name, wins: 0});
            }
        };
        teamsWins.sort(function (a, b) {
            return b.wins - a.wins;
        });
        return teamsWins;
    };

    render() {
        return(
            <div className='content'>
                <button className='button' onClick={this.getMainData}>Refresh content</button>
                <h2 className='heading-secondary'>Matches</h2>
                <Table tableName={'Matches'} tableData={this.state.matches} exludedKeys={'id'} uniqueKey={'id'} sortBy={this.sortBy}/>
                <h2 className='heading-secondary'>Teams</h2>
                <Table tableName={'Teams'} tableData={this.state.teams} uniqueKey={'name'} sortBy={this.sortBy}/>
            </div>            
        );
    };
}

export default Content;