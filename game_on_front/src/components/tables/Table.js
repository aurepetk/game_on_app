import React from 'react';
import './Table.css';

function Table({tableName, tableData, exludedKeys, uniqueKey, sortBy}) {
    const isTableEmpty = typeof tableData === undefined || tableData.length === 0;
    let tableKeys = isTableEmpty? [] : Object.keys(tableData[0]);
    let keys = tableKeys.filter(key => exludedKeys.indexOf(key) === -1);

    return (
        <table className='table'>
            <thead>
                <tr>
                    {keys.map(key => 
                        <th className='table__cell table__cell--header' key={key} onClick={() => sortBy(key, tableName)}>{key}</th>
                    )}
                </tr>
            </thead>
            <tbody>
                {
                    isTableEmpty ? 
                    <tr className='table__row'>
                        <td className='table__cell' colSpan={keys.length}>
                            Ops... Cannot get data from the server. Please refresh content.
                        </td>
                    </tr> 
                    :  tableData.map(rowData =>             
                        <tr className='table__row' key={rowData[uniqueKey]}>
                            {keys.map(key => 
                                <td className='table__cell' key={key}>{rowData[key]}</td>
                            )}
                        </tr>
                    )
                }
            </tbody>
        </table>
    );
};

Table.defaultProps = {
    exludedKeys: [],
    uniqueKey: 'id'
};

export default Table;