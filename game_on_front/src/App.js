import React from 'react';
import './App.css';
import Header from './components/layout/Header';
import Content from './components/layout/Content';
import Footer from './components/layout/Footer';

function App() {
  return (
    <div className="App">
      <Header headingText={'Game on!'}/>
      <Content 
        matchesURL={'http://gameon.test'} 
        teamsURL={'http://gameon.test/teams'} 
      />
      <Footer footerText={'Copyright © 2019 Genius Sports Group by Aurelija. All rights reserved.'}/>
    </div>
  );
}

export default App;